const {paginateResults} = require('./utils');

module.exports = {
    Query: {
        launches: async (parent, {pageSize = 20, after}, {dataSources}) => {
            const allLaunches = await dataSources.launchAPI.getAllLaunches();
            allLaunches.reverse();

            const launches = paginateResults({
                after,
                pageSize,
                results: allLaunches
            });

            console.log('our launches', launches);

            return {
                launches,
                cursor: launches.length ? launches[launches.length - 1].cursor : null,
                hasMore: launches.length
                    ? launches[launches.length - 1].cursor !==
                    allLaunches[allLaunches.length - 1].cursor
                    : false
            }
        },

        launch: (parent, {id}, {dataSources}) =>
            dataSources.launchAPI.getLaunchById({launchId: id}),
        me: async (parent, args, {dataSources}) =>
            dataSources.userAPI.findOrCreateUser()
    },
    Mission: {
        missionPath: (mission, {size} = {size: 'LARGE'}) => {
            return size === 'SMALL'
                ? mission.missionPatchSmall
                : mission.missionPatchLarge
        }
    },
    Launch: {
        isBooked: async (launch, args, { dataSources }) =>
            dataSources.userAPI.isBookedOnLaunch({launchId: launch.id})
    }
};